﻿Two years and thirteen days; two thousand miles; apart from my family,
friends, and even my wife; exiled from the country that raised me; barred from
college; forced to quit my job; wondering daily, ``How am I going to survive
this?" Absolutely, the worst time of my life. Being ordered to leave the
country nearly destroyed my life and dreams.

Now, I am back home with my wife, friends, and family. Since returning from
Mexico, I have worked as a call center representative, a survey worker, a bank
teller, a book seller, a programmer, and currently as a graduate student
researcher.  Nothing less than tremendous effort has been required to get back
on track financially and academically. Two years ago, I finally received my B.A.
in Economics with a minor in Computer Science. Now, I am a few weeks away 
from submitting my first computer science research paper.

% change this P to tie to research especially the first sentence
Paradoxically, I greatly appreciate the days I spent in Mexico because it was
there that my passion for computer science first sparked. First came three
months of job searching in a foreign, high-unemployment city. Within a year of
working as a call center representative, I grew into managing over eighty
representatives and their supervisors. Part of my management strategy involved
analyzing sales and quality data. Over many nights, I taught myself how to write
C++ and Visual Basic programs to process this data. For advanced database SQL
queries and processing, I recruited our whole IT department. I was nominated for
the Operations Manager of the Year award. By that point, the art and power of
programming had captivated me.  I wondered why companies did not require all
managers to learn programming.

% Continue here
On July 16, 2007, the US granted my permanent resident visa. That same night,
two years and thirteen days after arriving in Mexico, I walked across the border
into El Paso, TX, bringing with me a piece of luggage and a clearer career
vision. Before leaving the US, I had hoped to help Latin American countries
advance their economies through Economics research after earning my degree. I
was proud that as a manager I had helped people by creating and saving over 60
jobs. Once in the US, I wanted to replicate this success by pursuing
programming as a profession.

% Delete this P
For over a year after my return, I labored at three different jobs to restore
my financial health. Attorney fees, immigration fees, and travel expenses had
depleted my savings. In the fall of 2008, I re-enrolled at UC Davis to
continue learning programming.   

% Rewrite the first clause of this sentence
In the first day of school, in the first lecture of Discrete Mathematics, I
realized that I had made a huge mistake; not in direction but in magnitude. In
Mexico, I had not developed a passion for programming but for applied
mathematics, particularly, computer science. In this class, Professor Phillip
Rogaway taught us how to differentiate our Ps from our Qs (formal logic), how
to count (set theory), and how to connect the dots (graph theory). He dazzled
us with stories of how scientists solve research problems.  I realized then that
the mathematical and formal problem-solving aspects of programming were what had
captivated me.

Courses like Algorithms and Theory of Computation continued to feed my interest
in the open problems of Computer Science. Working as a programmer for Professor
Rand Conger at UC Davis further strengthened my interest and programming skills.
I felt prepared and eager to assist in research. After taking Programming
Languages and having done well in all of my computer science classes, I offered
my assistance to Professor Zhendong Su. I have worked with him as a research
assistant since. 

Working for the California Families research project (led by Professor Conger)
taught me that good science has purpose.  That study aimed to help policy makers
by increasing the understanding of Mexican-American families in California. I
want to research problems which are exciting to me, of course, but can also help
society.  The research I have outlined has this promise, and can potentially 
transform software development.

Though my journey has been difficult, I am not unique. In my hometown of
Stockton, CA alone there must be thousands of students with my background, 
similar aspirations, and facing similar challenges.  They can succeed but they
don't know that.  Many of them abandon their dreams of ever attending college.
That is why I invited a fellow Ph.D. student to accompany me to talk at my
former high school.  We shared our stories to over one hundred students.  We
offered our emails; we offered help and encouragement.  We were invited back and
will return again this year. 

% Expand a bit on current research and refer to RP
This is where my story meets the present. I am currently working on a research
project with Professor Su to rethink and redesign programming.  I plan to earn a
Ph.D. in Computer Science on this topic under his guidance.  The generous support
from an NSF Graduate Research Fellowship will help me carry out my research.  It
will give me much flexibility and help me focus on my research, not on the
nearest financial hurdles.  A few too many of those have already tripped me up
and slowed me down.  In the long run, I aspire to teach and continue research in
a faculty position to educate and mentor the future generations of engineers and
scientists. 

