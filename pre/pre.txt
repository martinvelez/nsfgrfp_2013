﻿As an undergraduate, I became a key support team member in a large-scale
sociology study.  I volunteered to assist in a challenging study of obfuscation
engines.  My passion for helping society through science fueled my efforts.  The
engineering and theory problems I faced strengthened my intellectual abilities.
Mentors taught me how to tackle problems as a scientist.  As a whole, my
experience has given me wisdom to identify an exciting research problem in
Computer Science, intellectual maturity to formulate a research plan, and
confidence to tackle it as a Ph.D. student.
 
\textbf{Undergraduate: California Families Project}

My exposure to research began at UC Davis as a part-time survey worker in the
fall of 2008.  I interviewed children and parents participating in the
California Families Project, an ongoing longitudinal child development study
following over six hundred Mexican-American California families. 

In the following two years, my role and responsibilities changed.  As a research
coordinator, I coordinated and supervised a team of approximately twenty survey
workers. As the lead programmer, I authored the survey software used by the
survey workers to perform interviews.  I rewrote the entire codebase to fix some
logic flaws and to make it easy to maintain and update. It is still in use.  I
also developed an internal web application to help coordinate family
appointments, and survey data.  All appointment and data tracking had been done
manually.  As a programmer, though, I was primarily responsible for ensuring
data quality.  Good research relies on good data.  Due to the importance of this
task, I worked closely with Professor Rand Conger, the principal investigator. I
also participated in research meetings in which I served as the technical expert
on all matters relating to survey data software, and data collection and
processing. 

This study is important because it promises to help shape American public policy
towards Mexican-Americans, the fastest growing minority in the United States.
Assisting in this scientific effort sharpened my leadership and programming
skills, and confirmed my lifelong interest in research. More importantly, I
learned that good science has more than intellectual merit; it tries to affect
society in a positive way.  

\textbf{Undergraduate: Obfuscation Engines}

Since the summer of 2009, I have been working with Dr. Earl Barr, I3P Fellow,
and UC Davis Professor Zhendong Su on a research project involving obfuscation
engines.  Obfuscation engines rewrite software while preserving its original
semantics.  Software developers use obfuscation engines to protect their code
from being reverse engineered.  Malware writers also exploit obfuscation to fool
anti-virus software.  In this project, I am assisting Dr. Barr in developing an
algorithm to extract the transformation rules implemented by any obfuscation
engine.  I have collected and reconstructed numerous real-world obfuscation
engines.  I have also built a framework to test these obfuscation engines with
our algorithm.  Dr. Barr has presented our preliminary results at the Institute
for Information Infrastructure Protection (I3P).  Due to the challenges of our
task, our work is not yet complete. 

Dr. Barr is now a Lecturer at University College London. We continue working
together and hope to culminate this project with a submission to a computer
security conference in 2013.  Our contribution would be an automatic algorithm
for inferring the transformation rules implemented by any obfuscation engine. 

In this experience, I have learned the importance of perseverance, properly
defining research problems and terms, and distinguishing between engineering
and theory.  Research throws up more obstacles than I had expected; these
obstacles are often real or apparent dead ends. The trick is to discover which
is which.  In either case, one must persevere.  For example, I have, on
occasion, spent weeks engineering a solution to a subproblem only to discover
later that the solution was flawed in theory.   

While working with Professor Su, I also participated in his research
meetings where he and his students discuss current work and explore new research
opportunities.  At first, I understood very little.  The mere terminology, as
well as the breadth and depth of the research, were barriers.  In addition to
learning new terminology and concepts, I learned how successful researchers
think about and attack problems.

\textbf{Graduate: Minimalistic Programming}

This fall, I started the Ph.D. Computer Science program at UC Davis.  Professor
Su is my advisor.  Under his guidance, I have started a research project I plan
to complete as a Ph.D. student.  I aim to rethink and redesign programming.  The
key observation of my research project is that programs contain much syntactic
chaff.  Chaff in programs has little or no semantic value to programmers.  It
can also signal weaknesses in programming language design.  My hypothesis is
that chaff can be greatly reduced.  I plan to test this hypothesis, and use it
as a guiding principle.  The expected result is a new minimalistic programming
model that will allow programmers to complete most programming tasks using a few
semantically-rich keywords.  I present more details on this research project in
my research proposal essay.



